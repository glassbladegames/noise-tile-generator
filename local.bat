rmdir /s /q built
docker build . -f builders/windows/Dockerfile -t noise-tiles-windows --build-arg CI_COMMIT_BRANCH="don't push" --build-arg DEBUG="-debug"
if %errorlevel% neq 0 exit /b %errorlevel%
docker create --name noise-tiles-windows noise-tiles-windows
docker cp noise-tiles-windows:/game/built built
docker rm noise-tiles-windows
cd built
noise-tiles.exe
